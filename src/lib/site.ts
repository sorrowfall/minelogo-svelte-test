import { scene, canvas, controls, camera } from './scene'
import { Text, Row, Model } from './generate'
import { parseModel } from './render'
import type { ModelData } from './types'

/* window.onchange = function() {
    clearScene()
    generateFromInputs()
} */

//var storage = window.localStorage

function save() {

}

const defaultData = {rows: [{texts: [{}]}]}

function generateInputs() {

    /* var data = storage.getItem('inputData') || defaultData

    for (const row of data.rows) {

        var rowObject = addRow(row)
        for (const text of row.texts) addText(rowObject, text)

        document.querySelector('#rowContainer').appendChild(rowObject)

    } */

}

//const rowTemplate = document.getElementById('rowTemplate')

/* function addRow(data) {
    var row = rowTemplate.content.cloneNode(true).children[0]

    if (data.model) row.querySelector('.model').querySelector('input').value = data.model

    return row
}

function addText(row, data) {
    var text = document.getElementById('textTemplate').content.cloneNode(true).children[0]

    if (data.text) text.querySelector('.text').value = data.text
    if (data.texture) text.querySelector('.texture').querySelector('input').value = data.texture
    if (data.color) text.querySelector('.textureColor').value = data.color

    row.appendChild(text)
} */

export function generateFromInputs() {

    /* const rows = document.getElementsByTagName('row')
    const rowList = []
    for (const row of rows) {

        const type = row.getElementsByClassName('model')[0].value
        //const position = row.getElementsByClassName('position')[0].value

        const parts = row.getElementsByTagName('text')
        const textList = []
        for (const part of parts) {

            const texture = part.getElementsByClassName('texture')[0].value
            const text = part.getElementsByClassName('text')[0].value || 'Minelogo'
            console.log(text)

            const finalText = new Text( text, type, texture )

            textList.push(finalText)

        }

        const finalRow = new Row( textList )//, position )
        rowList.push(finalRow)

    }

    //const finalModel = new Model( rowList ) */

    const finalModel = new Model([
        new Row([
            new Text('lz?{}')
        ])
    ])

    parseModel( finalModel.toJSON() as ModelData )
    scene.modelData = finalModel.toJSON()

}

function clearScene(object: any = scene) {

    for (var i = object.children.length - 1; i >= 0 ; i --) {
        clearScene(object.children[i])
    }

    if (object.type !== 'Mesh') return

    scene.remove(object)
    object.geometry.dispose()
    object.material.dispose()

}

// Button Functions

// TODO: find way to fix resolution upscaling before a screenshot
function screenshot() {
    //renderer.setPixelRatio( 16 )
    //camera.lookAt( new THREE.Vector3(0, 0, 0) )
    //renderer.render(scene, camera)
    //camera.lookAt( new THREE.Vector3(0, 0, 0) )

    const blob = canvas.toDataURL('image/png')

    //renderer.setPixelRatio( 4 )

    return blob
}

export function clipboardImage() {
    const blob = screenshot()

    if (!navigator.clipboard) {
        window.open( blob, 'screenshot' )
        return
    }
    navigator.clipboard.write([
        new ClipboardItem({
            'image/png': blob
        })
    ])
}

export function saveImage() {
    const blob = screenshot()

    let download = document.createElement('a')
    download.href = screenshot()
    download.download = 'title.png'
    download.style.display = 'none'

    document.body.appendChild(download)
    download.click()
    document.body.removeChild(download)
}

export function saveModel() {
    var json = JSON.stringify(scene.modelData)
    if (!json) return

    let download = document.createElement('a')
    download.href = 'data:text/plaincharset=utf-8,' + encodeURIComponent(json)
    download.setAttribute('download', 'title.mimodel')
    download.style.display = 'none'

    document.body.appendChild(download)
    download.click()
    document.body.removeChild(download)
}

export function zoomIn(scale = 0.2) {
    camera.zoom += scale
    controls.object.updateMatrixWorld(true)
    //controls.object.updateProjectionMatrix();
}

export function zoomOut(scale = 0.2) {
    camera.zoom -= scale
    controls.object.updateMatrixWorld(true)
    //controls.object.updateProjectionMatrix();
}

export function resetZoom() {
    controls.reset()
}