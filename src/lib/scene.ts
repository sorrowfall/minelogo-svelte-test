import * as THREE from 'three';

import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';

import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer.js';

import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass.js';
import { RenderPixelatedPass } from 'three/examples/jsm/postprocessing/RenderPixelatedPass.js';
import { SMAAPass } from 'three/examples/jsm/postprocessing/SMAAPass.js';

class MainScene extends THREE.Scene {
    modelData: object = {}
}

export var scene = new MainScene()

var ambientLight = new THREE.AmbientLight(0xbbbbbb)
scene.add(ambientLight)

export function setAfterEffects(style: 'pixel' | string) {

    switch (style) {

        case 'pixel':
            const renderPixelatedPass = new RenderPixelatedPass( 4, scene, camera )
            renderPixelatedPass.normalEdgeStrength = 0
            renderPixelatedPass.depthEdgeStrength = 0
            composer.addPass( renderPixelatedPass )
            break

        default:
            const renderPass = new RenderPass( scene, camera )
            composer.addPass( renderPass )
            const pass = new SMAAPass( window.innerWidth * renderer.getPixelRatio(), window.innerHeight * renderer.getPixelRatio() );
            composer.addPass( pass )

    }

}

let composer: EffectComposer
let renderer: THREE.WebGLRenderer

export let canvas: HTMLCanvasElement
export let camera: THREE.PerspectiveCamera
export let controls: OrbitControls

export function createScene(canvasElement: HTMLCanvasElement) {

    canvas = canvasElement // document.querySelector('#render')
    renderer = new THREE.WebGLRenderer( { canvas: canvas, antialias: false, alpha: true, preserveDrawingBuffer: true } )
    renderer.setPixelRatio( 4 )
    composer = new EffectComposer( renderer )

    camera = new THREE.PerspectiveCamera( 90, window.innerWidth/window.innerHeight, 1, 1000 )
    camera.position.set( 0, -50, 80 )
    controls = new OrbitControls( camera, canvas)
    controls.enablePan = false
    //controls.zoom = 100
    controls.minDistance = 64
    controls.maxDistance = 640

    //controls.object.zoom = 1
    //controls.object.updateProjectionMatrix()

    setAfterEffects('pixel')
    animate()

}

export function animate() {
    requestAnimationFrame(animate)
    composer.render()
}