import type { ModelData, ModelPart, Character, Array2, Array3 } from './types'
import fs from 'fs'

import title_model from '../data/title.json'

const modelFileMap: {[key: string]: ModelPart} = {
    title: title_model
}

console.log(title_model)

export var modelList: {[key: string]: ModelData} = {

    title: {
        characterDefaults: {
            '': {
                maxLength: 0,
                shapes: []
            },
            ' ': {
                maxLength: 16,
                shapes: []
            },
            'D': {
                maxLength: 32
            }
        },
        textureSize: [1000, 1000],
        textHeight: 44,
        textures: {
            classic: 'data/classic_title.png',
            flat: 'data/flat_title.png',
            modern: 'data/modern_title.png',
            mifamily: 'data/mifamily_title.png'
        },
        characters: {}
    }

    //subtitle

}

for (var model_name in modelList) {

    var model: ModelData = modelList[model_name]

    const data = modelFileMap[model_name]

    model.characters = getShapes({}, data)

    for (const char_name in model.characters) {
        model.characters[char_name].maxLength = widestCube( model.characters[char_name] )
    }
    for (const char_name in model.characterDefaults) {
        model.characters[char_name] = { ...model.characters[char_name] || {}, ...model.characterDefaults[char_name] }
    }

}

function getShapes(characters: {[key: string]: Character}, model: ModelPart) {
    if (model.shapes) {
        var character = model as Character
        character.maxLength = 0
        characters[model.name] = character
        return characters
    }
    for (const part of model.parts) {
        getShapes(characters, part)
    }
    return characters
}

function widestCube(model: ModelPart): number {
    var widths = [0]
    for (var cube of model.shapes) {
        if (cube.type == 'block') {
            widths.push( Math.abs(cube.from[0]) + Math.abs(cube.to[0]) )
        }
    }
    return Math.max(...widths)
}

function isOdd(number: number) {
    return number % 2
}

var rowI = 0
export class Model {

    rows: Array<Row>

    textHeight: number

    constructor(rows = [new Row()]) {

        this.rows = rows

        var modelHeight = 0

        var multiplier = 1
        if (!isOdd(this.rows.length)) {
            var multiplier = 0.75
        }

        for (var row of this.rows) {
            modelHeight += row.model.textHeight * multiplier
        }

        this.textHeight = 0
        for (var row of this.rows) {
            row.name = "Row #"+rowI.toString()
            rowI++
            modelHeight -= row.model.textHeight
            row.position[1] = modelHeight

            switch (row.textPosition) {
                case 'left':
                    // TODO
                    break
                case 'right':
                    // TODO
                    break
            }

        }

    }

    toJSON() {

        var final = {
            name: 'MineLogo Model',
            parts: []
        }

        for (const rowI in this.rows) {
            const row = this.rows[rowI]

            var parts = []

            for (const textI in row.texts) {
                const text = row.texts[textI]

                parts.push({
                    name: text.name,
                    position: text.position,
        
                    show_position: true,
        
                    texture: text.texture,
                    texture_size: text.textureSize,
        
                    parts: text.shapes
                })
            }

            var rowData = {
                name: row.name,
                position: row.position,
                parts: parts
            } as never // ???

            final.parts.push(rowData)

        }

        return final

    }

}

type Position = 'left' | 'center' | 'right'

var textI = 0
export class Row {

    texts: Array<Text>
    textPosition: Position
    model: ModelData

    name: string = ''
    textLength: number
    position: Array3

    constructor(texts = [new Text()], model=modelList['title'], textPosition = 'center') {
        this.texts = texts
        this.textPosition = textPosition as Position
        this.model = model

        this.textLength = 0
        for (var text of this.texts) {
            text.name = "Text #"+textI.toString()
            textI++
            text.position = [this.textLength, 0, 0]
            this.textLength += text.textLength
        }

        this.position = [Math.round(this.textLength / -2), 0 ,0]

    }

}

var characterI = 0
export class Text {

    name: string
    model: ModelData
    texture: string
    textureSize: Array2

    shapes: Array<object>
    textLength: number
    startingPoint: number
    position: Array3

    constructor(text = 'minelogo', model = 'title', texture = 'classic') {
        this.name = text
        this.model = modelList[model]
        this.texture = this.model.textures[texture]
        this.textureSize = this.model.textureSize

        const textList = text.toUpperCase().split('')

        // remove nonexistent chars
        var i = textList.length
        while (i--) {
            if ( textList[i] in this.model.characters ) continue
            textList.splice(i, 1)
        }

        // calculate text length
        this.textLength = 0
        for (const char of textList) this.textLength += this.model.characters[char].maxLength
        this.startingPoint = Math.round(this.textLength / -2)

        // calculate shape positions
        this.shapes = []
        this.textLength = 0
        for (const char of textList) {
            const part = this.model.characters[char]

            var partCopy = JSON.parse(JSON.stringify(part))
            partCopy.position = [this.textLength + (partCopy.maxLength/2), 0, 0]
            this.textLength += partCopy.maxLength

            i = 0
            partCopy.name = 'Char #'+characterI.toString()
            characterI++

            this.shapes.push(partCopy)
            this.startingPoint += part.maxLength
        }

        this.position = [Math.round(this.textLength / -2), 0, 0]

    }

}